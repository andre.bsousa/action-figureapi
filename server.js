const express = require('express')
const app = express()
const cors = require('cors')
const PORT = process.env.PORT || 8080
const bodyParser = require('body-parser')
const connectDB = require('./config/db')

app.use(cors())
app.use(express.json())
app.unsubscribe(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

connectDB()


//Rotas
app.use('/', require('./routes/hello'))
app.use('/user', require('./routes/api/user'))

app.listen(PORT, () => console.log(`Rodando em: http://localhost:${PORT}`))