const MSGS = {
    'EMAIL_DUPLICATED': 'Email duplicado',
    'GENERIC_ERROR' : 'Aconteceu algo inexperado',
    'USER404' : 'Usuario não encontrado'
}

module.exports = MSGS