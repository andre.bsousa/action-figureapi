const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')
const { check, validationResult } = require('express-validator')
const User = require('../../models/user')
const MSGS = require('../../messages')


// @route POST/user
// @desc CREATE user
// @acess Public
router.post('/',[
    check('email', 'email invalido').isEmail(),
    check('name', 'nome invalido').not().isEmpty(),
    check('password', 'Por favor insira uma senha com 5 ou mais caracteres').isLength({min: 5})
], async (req, res, next) => {
    
    try{

        let {name, email, password } = req.body
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).json({ erros: errors.array() })
        } else {
            let validarUsuario = await User.findOne({ email })
            if(validarUsuario){
                res.status(409).send({ "error": MSGS.EMAIL_DUPLICATED })
            } else {
                let user = new User({ name, email, password})
                const salt = await bcrypt.genSalt(10)
                user.password = await bcrypt.hash(password, salt)
                await user.save()
                if(user.id){
                    res.json(user)
                }
            }
        }

    } catch (error){
        console.error(error.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }

})

// @route GET/user
// @desc LIST/user
// @acess Public
router.get('/', async (req, res, next) => {
    try {

        const user = await User.find({})
        if(user){
            res.json(user)
        } else {
            res.status(404).json({"error": MSGS.USER404})
        }        

    } catch (error){
        console.error(error.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }

})

// @route DELETE/user
// @desc DELETE/user
// @acess Public
router.delete('/:userId', async (req, res, next) => {
    try {

        const id = req.params.userId

        const user = await User.findByIdAndDelete({ _id: id })

        if(user){
            res.json(user)
        } else {
            res.status(404).json({ "error": MSGS.USER404 })
        }

    } catch (error){
        console.error(error.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})

// @route PATCH/user
// @desc PARTIAL UPDATE/userId
// @acess Public
router.patch('/:userId',[
    check('email', 'email invalido').isEmail(),
    check('name', 'Nome invalido').not().isEmpty(),
    check('password', 'Digite uma senha de no minimo 5 caracteres').isLength({ min: 5 })
], async (req, res, next) => {
    try {
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).send({ erros: errors.array() })
        }

        const id = req.params.userId
        const salt = await bcrypt.genSalt(10)
        const bodyRequest = req.body

        if(bodyRequest.password){
            bodyRequest.password = await bcrypt.hash(bodyRequest.password, salt)
        }

        const update = { $set: req.body }
        const user = await User.findByIdAndUpdate(id, update, { new: true })
        if(user){
            res.send(user)
        } else {
            res.status(404).send({"error": MSGS.USER404})
        }


    } catch (error) {
        console.error(error.message)
        res.status(500).send({ "error": MSGS.GENERIC_ERROR})
    }


})


module.exports = router