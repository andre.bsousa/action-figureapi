const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true
    },

    brand: {
        type: String,
        require: true
    },

    company: {
        type: String,
        require: true
    },

    description: {
        type: String
    },

    type: {
        type: String,
        enum: ["Stantard", "Collector"],
        require: true
    },

    price: {
        type: Number,
        require: true
    },

    features: {

        size: {
            type: Number,
            require: true
        },

        material: {
            type: String,
            require: true
        },

        quantity: {
            type: Number,
            require: true
        }
    },

    content: [String]

})

module.exports = mongoose.model('Product', productSchema)